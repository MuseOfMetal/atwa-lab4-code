package org.example;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import org.apache.http.HttpStatus;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.Map;

import static io.restassured.RestAssured.given;

public class LabThree
{
    private static final String baseUrl = "https://907dee84-a913-497d-a980-4204714c258e.mock.pstmn.io/";
    //private Integer orderId;

    @BeforeClass
    public void setup(){
        RestAssured.basePath=baseUrl;
        RestAssured.defaultParser= Parser.JSON;
        RestAssured.requestSpecification = new RequestSpecBuilder().setContentType(ContentType.JSON).build();
        RestAssured.responseSpecification = new ResponseSpecBuilder().build();
    }


    @Test
    public void testGetSuccess()
    {
        Response response = given().get(baseUrl+"owner/name/success");
        response.then().statusCode(HttpStatus.SC_OK);
    }
    @Test
    public void testGetUnsuccess()
    {
        Response response = given().get(baseUrl+"owner/name/unsuccess");
        response.then().statusCode(HttpStatus.SC_FORBIDDEN);
    }
    @Test
    public void testPostSuccess()
    {
        Response response = given().post(baseUrl+"createsomething?perm=yes");
        response.then().statusCode(HttpStatus.SC_OK);
    }
    @Test
    public void testPostUnsuccess()
    {
        Response response = given().post(baseUrl+"createsomething");
        response.then().statusCode(HttpStatus.SC_FORBIDDEN);
    }
    @Test
    public void testPut()
    {
        Map<String, ?> body = Map.of(
                "name", "Artyom",
                "surname", "Kovalyov"
        );

        Response response = given().body(body).put(baseUrl+"updateMe");
        response.then().statusCode(HttpStatus.SC_OK);
    }
    @Test
    public void testDelete()
    {
        Response response = given().header("SessionID","123456789").delete(baseUrl+"deleteWorld");
        response.then().statusCode(HttpStatus.SC_GONE);
    }
}
